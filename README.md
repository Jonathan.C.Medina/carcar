# CarCar

Team:
* Gabe Svetcos (@Gsvetcos on GitLab and Github) - Service Microservice
* Jonathan Ceasar Medina(@Jonathan.C.Medina on Gitlab / @JonathanCMedina on Github) - Sales Microservice

## Vision of the project
Project Beta (CarCar) is developed to emulate a car dealership website. This project uses three microservices (Inventory, Sales, and Service) with interactivity between each.

Within each microservice are RESTful API patterns used to Create, Read, Update, and Delete information through the use of Graphical Human Interface applications (such as sending JSON Body requests in Insomnia). The front-end is presented through a SPA (single-page application) created with React.js.
Other programs used to create this application are:
1. Bootstrap for front-end styling
2. Python and Django framework web programming for website creation

## Instructions on running the application
### Prior to running the application, you will want to download a few applications/tools:
* VSCode (or a similar IDE/Integrated Development Environment)
* Docker desktop (to create volumes, images, and containers to run the application)
* Insomnia (to test back-end functionality)
* A web-based browser (preferrably Google Chrome) 

### Once those requirements are met:
* Turn on Docker desktop
* Go to https://gitlab.com/Gsvetcos/project-beta then fork and clone the repository
* Navigate into the `project-beta` folder then open VSCode or your preferred IDE
* To run the program, type the following commands in the terminal:
    * `docker volume create beta-data`
    * `docker-compose build`
    * `docker-compose up`

Please feel free to start with http://localhost:3000. You will be met with the main page of CarCar.

**You may need to wait until the terminal says it has compiled successfully. This may take up to five minutes depending on your device's specifications**
### Additional Information
The server should be running once the http://localhost:3000 page loads by itself. You will have access to the React URLs in the front end (the links starting with http://localhost:3000/), as well as the backend URLs (the links starting with http://localhost:8100/api/ http://localhost:8080/api/ http://localhost:8090/api/). A full list of URLs will be in the API Documentation section.

For testing purposes, please begin by creating a new manufacturer, then a new model, then a new automobile. 
Following the structure of the API Documentation should guide you with what needs to be created before proceeding with either scheduling a service appointment or making a new sale. 

## Diagram
![Gabe and JC's DDD Diagram of Project Beta / CarCar!](images/design-diagram.png "DDD Diagram of Project Beta / CarCar")

## API Documentation

### JSON Body Examples
Below are examples of the JSON Body content if you wish to test some requests in Insomnia:
| Method | Link |
| --- | --- |
|POST | http://localhost:8080/api/appointments/ 

JSON Body example to be input and sent:
```
{
    "vin": "1C3CC5FB2AN120000",
    "customer": "Erick Svetcos",
    "date_time": "2023-07-25T01:44:34+00:00",
    "reason": "Headlight Replacement",
    "technician_id": 1
}
```

| Method | Link |
| --- | --- |
|PUT | http://localhost:8100/api/models/2/

JSON Body example to be input and sent when updating automobile model information:
```
{
    "name": "Civic",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg/280px-2017_Honda_Civic_SR_VTEC_1.0_Front.jpg"
}
```

| Method | Link |
| --- | --- |
|GET | http://localhost:8090/sales/

JSON Body example of what is retrieved when requesting the full list of sales:
```
{
	"sales": [
		{
			"id": 1,
			"price": 0,
			"automobile": {
				"id": 1,
				"vin": "abc",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "qqq",
				"last_name": "www",
				"employee_id": "eee"
			},
			"customer": {
				"id": 1,
				"first_name": "111",
				"last_name": "222",
				"address": "333",
				"phone_number": "444"
			}
		},
		{
			"id": 2,
			"price": 99999,
			"automobile": {
				"id": 2,
				"vin": "ABCDEFGHIJ1234567",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "qqq",
				"last_name": "www",
				"employee_id": "eee"
			},
			"customer": {
				"id": 1,
				"first_name": "111",
				"last_name": "222",
				"address": "333",
				"phone_number": "444"
			}
		}
	]
}
```


| Method | Link |
| --- | --- |
|DELETE | http://localhost:8090/salespeople/2/

JSON Body example of what is retrieved after deleting a salesperson:
```
{
	"deleted": true
}
```

#### Additional Notes on Insomnia testing
If you would like to test the other GET/POST/PUT/DELETE methods, please refer to the next section for more information on how the JSON body needs to be formatted.

To get a clearer idea of what input in the JSON body requests, please refer to the properties in the models.py files. 
The exceptions to this rule are models with properties that have a default value such as the `status` property in the Appointment model of the Service microservice. 
Those with default states do NOT need to be input for your JSON body inputs. These are automatically generated after you create a POST request. 


### Frontend URL Paths and use description for Inventory Microservice
Below are the URL Paths of front-end RESTful API URL patterns and their intended use
| URL Path                                  |                    Description                                                                                  |
| ----------- | ----------- |
| http://localhost:3000/manufacturers       | Lists all manufacturers with first name, last name, and employee ID                                             |
| http://localhost:3000/manufacturers/new   | Add a new automobile manufacturer with the manufacturer's name                                                  |
| http://localhost:3000/models              | Lists all automobile models with a picture example of the vehicle                                               |
| http://localhost:3000/models/new          | Add a new automobile model, type in the name, add a picture URL (optional) and choose a manufacturer            |
| http://localhost:3000/automobiles         | Lists all automobiles in the inventory with the VIN, color, year, model, manufacturer, and sold status          |
| http://localhost:3000/automobiles/new     | Add a new automobile, type in the VIN,color, year, and select an existing model from the inventory              |

### Backend URL Paths and use description for Inventory Microservice
Below are the URL Paths of back-end URL RESTful API URL patterns and their intended use
| URL Path                                     |                    Description                                                                               |
| ----------- | ----------- |
| http://localhost:8100/api/automobiles/       | Lists all automobiles + Can also be used to POST info through a json body                                    |
| http://localhost:8100/api/automobiles/<str:vin>/| Can retrieve, delete, or update a specific automobile given their VIN                                     |
| http://localhost:8100/api/manufacturers/     | Lists all manufacturers + Can also be used to POST/create a new manufacturer through a JSON body             |
| http://localhost:8100/api/manufacturers/<int:pk>| Used to retrieve, delete, or update a specific manufacturer given their database ID                       |
| http://localhost:8100/api/models/            | Lists all models, can be used to POST/create a new model through a JSON body                                 |
| http://localhost:8100/api/models/<int:pk>/   | Used to retrieve, delete, or update a specific automobile model given their database ID                      |

### Frontend URL Paths and use description for Services Microservice
Below are the URL Paths of front-end RESTful API URL patterns and their intended use
| URL Path                                  |                    Description                                                                                  |
| ----------- | ----------- |
| http://localhost:3000/technicians         | Lists all technicians with first name, last name, and employee ID                                               |
| http://localhost:3000/technicians/new     | Add a new technician with first name, last name, and employee ID                                                |
| http://localhost:3000/appointments        | Lists active appointments with info about the customer, technician, reason, VIP status, VIN, & appt status      |
| http://localhost:3000/appointments/new    | Create a new service appointment given VIN, customer info, appointment date and time, reason, and technician    |
| http://localhost:3000/appointments/history| Lists all service appointments ever created with a search bar to filter info by VIN                             |

### Backend URL Paths and use description for Services Microservice
Below are the URL Paths of back-end URL RESTful API URL patterns and their intended use
| URL Path                                     |                    Description                                                                               |
| ----------- | ----------- |
| http://localhost:8080/api/technicians/       | Lists all technicians + Can also be used to POST info through a json body                                    |
| http://localhost:8080/api/technicians/<int:pk>/| Retrieves information about a specific technician given their assigned ID in the database                  |
| http://localhost:8080/api/appointments/      | Lists active appointments with info about the customer, technician, reason, VIP status, VIN, & appt status   |
| http://localhost:8080/api/appointments/      | Lists all active appointments + creates a new appointment                                                    |
| http://localhost:8080/api/appointments/<int:pk>/| Retrieves detail information about a specific appointment using a given id/pk                             |
| http://localhost:8080/api/appointments/<int:pk>/cancel/ | Cancels a specific appointment at a given id/pk                                                   |
| http://localhost:8080/api/appointments/<int:pk>/finish/ | Marks a specific appointment the "finished" status given id/pk                                             |

### Frontend URL Paths and use description for the Sales Microservice
Below are the URL Paths of front-end RESTful API URL patterns and their intended use
| URL Path                                  |                    Description                                                                                  |
| ----------- | ----------- |
| http://localhost:3000/salespeople         | Lists all salespeople with first name, last name, and employee ID                                               |
| http://localhost:3000/salespeople/new     | Create a new salesperson with first name, last name, and employee ID                                            |
| http://localhost:3000/customers           | Lists all customers with first name, last name, address, and phone number                                       |
| http://localhost:3000/customers/new       | Add a new customer with first name, last name, address, and phone number                                        |
| http://localhost:3000/sales               | Lists all sales with info of salespeople, customers, automobile VIN, and price of sale                          |
| http://localhost:3000/sales/new           | Create a new sale with dropdowns for an unsold automobile, a salesperson, and a customer. Input the price       |

### Backend URL Paths and use description for the Sales Microservice
Below are the URL Paths of back-end URL RESTful API URL patterns and their intended use
| URL Path                                      |                Description                                                                                  |
| ----------- | ----------- |
| http://localhost:8090/api/salespeople/        | Lists all salespeople + Can also be used to POST/create info through a json body                            |
| http://localhost:8090/api/salespeople/<int:pk>/ | Used to delete a specific salesperson given their database ID                                              |
| http://localhost:8090/api/customers/          | Lists all customers and can be used to POST/create info through a json body                                 |
| http://localhost:8090/api/customers/<int:pk>/ | Used to delete a specific customer given its database ID                                                    |
| http://localhost:8090/api/sales/              | Lists all info about sales made. Has all information about automobiles, customers, salespeople, and price   |
| http://localhost:8090/api/sales/<int:pk>/     | Used to delete a specific sale given its database ID.                                                       |

## Value Objects and Models
### Service Microservice Models
The models used within the Service microservice are:
* AutomobileVO
    * AutomobileVO is a value object model that retrieves information from the Inventory microservice using a poller. AutomobileVO is used for determining whether a vehicle that is scheduled for service was sold from the dealership; if so then that vehicle is considered a VIP.
* Technician
    * Technicians are employees within this dealership that work with the vehicles. Technicians have names and employee IDs
* Appointment
    * Appointments are created to keep track of the vehicles that need a service performed. In order to make an appointment, there must be a reason, the date and time that the appointment is scheduled (optional, customers can do walk-ins), the automobile's VIN, and information about the customer. Once an appointment is assigned, it is defaulted to the "created" status. This status can be manually changed when it meets one of the two conditions: the appointment's service is completed (the status changes to "finished") and the appointment is cancelled (status is changed to "cancelled").

### Sales Microservice Models 
The models used within the Sales Microservice are:
* AutomobileVO
    * AutomobileVO is a value object model that retrieves database information from the Inventory microservice using a poller. In the Sales microservice, the AutomobileVO is used to update the "sold" property of an automobile in the Inventory database. To do this, we implemented a "put" method within the submission of a sales form. 
* Salesperson 
    * Salesperson is a model for employees that work in sales to sell cars. The salesperson model takes in first name, last name, and employee ID to be created.
* Customer 
    * The customer model is used to determine contact information about a purchaser of a vehicle. The customer model uses first name, last name, address, and phone number as contact information.
* Sale
    * The sale model utilizes foreign keys to retrieve information related to the automobile, the salesperson, and the customer. The sale model also requires the price property to determine how much a vehicle costs. 


---------------------------------------------------------------------------------------------------------------------------------------------------------------
